package com.example.firebaseautenticacao;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private Button btn_login, btn_cadastro;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_login = findViewById(R.id.button_login);
        btn_cadastro = findViewById(R.id.button_cadastrar);
        init();
    }

    private void init() {

        auth = FirebaseAuth.getInstance();
        estadoAuthenticacao();
        btn_cadastro.setOnClickListener(cadastro());
        btn_login.setOnClickListener(login());
    }

    private void estadoAuthenticacao() {

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){

                    Toast.makeText(getBaseContext(), "Usuario " + user.getEmail() + "está logado",
                            Toast.LENGTH_LONG).show();
                }else{

                }

            }
        };
    }

    private View.OnClickListener login() {

        return view->{


            user = auth.getCurrentUser();

            if(user == null){

                Intent i  = new Intent(this, LoginEmailActivity.class);
                startActivity(i);

            }else{

                startActivity(new Intent(this, PrincipalActivity.class));
            }

        };
    }

    private View.OnClickListener cadastro() {

        return view ->{

            Intent i  = new Intent(this, CadastrarActivity.class);
            startActivity(i);
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(authStateListener != null){
            auth.removeAuthStateListener(authStateListener);
        }
    }
}

package com.example.firebaseautenticacao;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class CadastrarActivity extends AppCompatActivity {

    private EditText edt_email, edt_senha, edt_confirmar;
    private Button btn_cadastrar, btn_cancelar;
    private FirebaseAuth auth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        edt_email = findViewById(R.id.edt_EmailCadastro);
        edt_senha = findViewById(R.id.edt_SenhaCadastro);
        edt_confirmar = findViewById(R.id.edt_RepetirSenhaCadastro);
        btn_cadastrar = findViewById(R.id.btn_cadastrar);
        btn_cancelar = findViewById(R.id.btn_cancelar);

        auth = FirebaseAuth.getInstance();

        init();
    }


    private void init(){

        btn_cadastrar.setOnClickListener(clickCadastrar());
    }

    private View.OnClickListener clickCadastrar() {
        return view ->{
                cadastrar();
        };
    }

    private void cadastrar(){


        String email  = edt_email.getText().toString().trim();
        String senha   = edt_senha.getText().toString().trim();
        String confirmar = edt_confirmar.getText().toString().trim();

        if(email.isEmpty() || senha.isEmpty() || confirmar.isEmpty()){

            Toast.makeText(this, "Campos vazios", Toast.LENGTH_LONG).show();
        }


        if(senha.contentEquals(confirmar)){

            if(verificarInternet()){

                criarUser(email,senha);

            }else{

                Toast.makeText(this, "Sem Internet", Toast.LENGTH_LONG).show();
            }

        }else{

            Toast.makeText(this, "Senha incompativeis", Toast.LENGTH_LONG).show();
        }

    }

    private void criarUser(String email, String senha) {


        auth.createUserWithEmailAndPassword(email, senha)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){

                            Toast.makeText(getBaseContext(), "Cadastro efetuado com sucesso !", Toast.LENGTH_LONG).show();

                        }else{


                            opçoesErro(task.getException().toString());
                            //Toast.makeText(getBaseContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void opçoesErro(String resposta) {

        if(resposta.contains("least 6 characters")){

            Toast.makeText(getBaseContext(), "Digite uma senha maior que 5 characters", Toast.LENGTH_LONG).show();

        }else if(resposta.contains("address is badly")){

            Toast.makeText(getBaseContext(), "Endereço de email invalido", Toast.LENGTH_LONG).show();
        }

    }


    private boolean verificarInternet(){

        ConnectivityManager conexao = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo informacao = conexao.getActiveNetworkInfo();

        if(informacao != null &&  informacao.isConnected()){

            return true;

        }else{

            return false;
        }

    }

}

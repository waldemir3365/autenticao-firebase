package com.example.firebaseautenticacao;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginEmailActivity extends AppCompatActivity {

    private EditText edt_email, edt_senha;
    private Button btn_login, btn_recuperarSenha;
    private FirebaseAuth auth;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginemail);

        edt_email = findViewById(R.id.edt_Emaillogin);
        edt_senha = findViewById(R.id.edt_SenhaLogin);
        btn_login = findViewById(R.id.btn_okLogin);
        btn_recuperarSenha = findViewById(R.id.btn_recuperar);
        init();
    }


    private void init(){
        auth = FirebaseAuth.getInstance();
        btn_login.setOnClickListener(sign());
    }

    private View.OnClickListener sign() {

        return view->{

            loginEmail();
        };
    }

    private void loginEmail() {


        String email = edt_email.getText().toString().trim();
        String senha = edt_senha.getText().toString().trim();


        if (email.isEmpty() || senha.isEmpty()) {

            Toast.makeText(getBaseContext(), "Campos vazios", Toast.LENGTH_LONG).show();

        } else {


            confirmarLoginEmail(email, senha);

        }
    }
        private void confirmarLoginEmail (String email, String senha){

            auth.signInWithEmailAndPassword(email, senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if (task.isSuccessful()) {
                        startActivity(new Intent(getBaseContext(), PrincipalActivity.class));
                        Toast.makeText(getBaseContext(), "Usuario logado com sucesso !@", Toast.LENGTH_LONG).show();
                        finish();

                    } else {
                        Toast.makeText(getBaseContext(), "Erro ao logar", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

    }


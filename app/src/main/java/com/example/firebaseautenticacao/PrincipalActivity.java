package com.example.firebaseautenticacao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class PrincipalActivity extends AppCompatActivity {

    private Button btn_deslogar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btn_deslogar = findViewById(R.id.btn_deslogar);
        init();
    }

    private void init(){

        btn_deslogar.setOnClickListener(logout());
    }

    private View.OnClickListener logout() {
        return view->{

            FirebaseAuth.getInstance().signOut();
            finish();
        };
    }

}
